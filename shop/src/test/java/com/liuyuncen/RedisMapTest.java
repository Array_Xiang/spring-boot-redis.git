package com.liuyuncen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * <a href="https://www.cnblogs.com/daohangtaiqian/p/16533756.html">参考地址</a>
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-02  17:33
 * @description: TODO
 * @version: 1.0
 */
@SpringBootTest
public class RedisMapTest {



    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Test
    void mapSet(){
        // 传递一个map单个key
        redisTemplate.opsForHash().put("Map:A","key","value");
        Map<String,Object> map = new HashMap<>();
        map.put("key1","value1");
        map.put("key2","value2");
        // 传递一个map中多个key
        redisTemplate.opsForHash().putAll("Map:B",map);
    }

    @Test
    void mapGetAll(){
        Map<Object, Object> entries = redisTemplate.opsForHash().entries("Map:B");
        Object value = redisTemplate.opsForHash().get("Map:B", "value");
    }

    @Test
    void mapPut(){
        redisTemplate.opsForHash().put("Map:B","hello","world");
    }

    @Test
    void mapDelEntity(){
        redisTemplate.opsForHash().delete("Map:B","key1");
    }

    @Test
    void hasMap(){
        Boolean flag = redisTemplate.opsForHash().hasKey("Map:B", "hi");
    }

    @Test
    void mapIncr(){
        redisTemplate.opsForHash().put("Map:C","int",10);
        int data = Integer.parseInt(String.valueOf(redisTemplate.opsForHash().get("Map:C","int")));
        System.out.println("data = " + data);
        redisTemplate.opsForHash().increment("Map:C", "int", 3);
        data = Integer.parseInt(String.valueOf(redisTemplate.opsForHash().get("Map:C","int")));
        System.out.println("data = " + data);
        redisTemplate.opsForHash().increment("Map:C", "int", -6);
        data = Integer.parseInt(String.valueOf(redisTemplate.opsForHash().get("Map:C","int")));
        System.out.println("data = " + data);
    }


}
