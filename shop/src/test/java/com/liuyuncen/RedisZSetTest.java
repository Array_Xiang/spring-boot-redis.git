package com.liuyuncen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-05  16:42
 * @description: TODO
 * @version: 1.0
 */
@SpringBootTest
public class RedisZSetTest {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;



    @Test
    void zsetAdd(){
        redisTemplate.opsForZSet().add("ZSet:A","Hello9-1",9);
        redisTemplate.opsForZSet().add("ZSet:A","Hello9-2",9);
        redisTemplate.opsForZSet().add("ZSet:A","Hello9-3",9);
        redisTemplate.opsForZSet().add("ZSet:A","Hello9-4",9);
    }

    @Test
    void zsetGet(){
        Set<Object> range = redisTemplate.opsForZSet().range("ZSet:A", 0, -1);
        for (Object o : range) {
            System.out.println("data = " + o);
        }
    }

    @Test
    void zsetRank(){
        Set<Object> objects = redisTemplate.opsForZSet().rangeByScore("ZSet:A", 2, 3);
        for (Object object : objects) {
            System.out.println("data = " + object);
        }
    }
}
