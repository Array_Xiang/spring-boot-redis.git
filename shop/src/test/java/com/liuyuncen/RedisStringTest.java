package com.liuyuncen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

@SpringBootTest
class RedisStringTest {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * 普通缓存存入
     */
    @Test
    void setValue(){
        redisTemplate.opsForValue().set("String:A","Hello");
    }

    /**
     * 普通取出
     */
    @Test
    void getValue(){
        String value = String.valueOf(redisTemplate.opsForValue().get("String:A"));
        System.out.println("value = " + value);
    }

    /**
     * 存入并设置时间
     */
    @Test
    void setByTime(){
        redisTemplate.opsForValue().set("String:Time","10秒消失",10, TimeUnit.SECONDS);
    }

    /**
     * 递增
     */
    @Test
    void incr(){
        redisTemplate.opsForValue().set("Integer:A",10L);
        redisTemplate.opsForValue().increment("Integer:A",1L);
    }

    @Test
    void del(){
        redisTemplate.delete("Integer:A");
    }


}
