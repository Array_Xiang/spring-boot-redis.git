package com.liuyuncen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-05  16:43
 * @description: TODO
 * @version: 1.0
 */
@SpringBootTest
public class RedisSetTest {

    @Autowired
    RedisTemplate<String,Object> redisTemplate;


    @Test
    void setAdd(){
        redisTemplate.opsForSet().add("Set:A","key1");
    }

    @Test
    void setGet(){
        Long size = redisTemplate.opsForSet().size("Set:A");
        System.out.println("size = " + size);
        Set<Object> members = redisTemplate.opsForSet().members("Set:A");
        for (Object member : members) {
            System.out.println(member);
        }
    }

    @Test
    void setInter(){
        // Set:A 有 1、2、3   Set:B 有 2、3、4  差异是 2、3
        Set<Object> intersect = redisTemplate.opsForSet().intersect("Set:A", "Set:B");
        for (Object o : intersect) {
            System.out.println(o);
        }
    }
    @Test
    void setDiff(){
        // Set:A 有 1、2、3   Set:B 有 2、3、4  差异是 1、4
        Set<Object> difference = redisTemplate.opsForSet().difference("Set:B", "Set:A");
        for (Object o : difference) {
            System.out.println(o);
        }
    }

    @Test
    void setUnion(){
        Set<Object> union = redisTemplate.opsForSet().union("Set:A", "Set:B");
    }

    @Test
    void setHas(){
        Boolean member = redisTemplate.opsForSet().isMember("Set:A", 1);
    }

}
