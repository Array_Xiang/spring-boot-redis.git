package com.liuyuncen;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-05  16:43
 * @description: TODO
 * @version: 1.0
 */
@SpringBootTest
public class RedisListTest {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;


    @Test
    void listAdd(){
        redisTemplate.opsForList().leftPushAll("List:A",1,2,3);
        redisTemplate.opsForList().rightPushAll("List:B",1,2,3);
    }

    @Test
    void listGet(){
        List<Object> range = redisTemplate.opsForList().range("List:B", 0, 1);
        for (Object o : range) {
            System.out.println("data = " + o);
        }
    }

    @Test
    void listUpdate(){
        redisTemplate.opsForList().set("List:B",2,"Hello");
    }

    @Test
    void listDel(){
        redisTemplate.opsForList().remove("List:B",-1,1);
    }

    @Test
    void listPop(){
        Long size = redisTemplate.opsForList().size("List:B");
        for (int i = 0; i < size; i++) {
            System.out.println("data = " + redisTemplate.opsForList().leftPop("List:B"));
        }
        System.out.println("当 List:B 没有数据了，再继续Pop取出");
        System.out.println("data = " + redisTemplate.opsForList().leftPop("List:B"));
    }

}
