package com.liuyuncen.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.shop.controller
 * @author: Xiang想
 * @createTime: 2022-09-05  16:59
 * @description: TODO
 * @version: 1.0
 */
@RestController
public class ShopController {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate<String ,Object> redisTemplate;


    /**
     * @description: 创建一个库存量为200的商品
     * @author: Xiang想
     * @date: 2022/9/5 5:00 PM
     * @param: []
     * @return: java.lang.String
     **/
    @RequestMapping("/setStock")
    public String setStock(){
        stringRedisTemplate.opsForValue().set("stock",200+"");
        return "ok";
    }

    @GetMapping("/setListStock")
    public String setListStock(){
        redisTemplate.delete("stock:list");
        int stock = 300;
        for (int i = 0; i < stock; i++) {
            String stockName = "菜死我算了 - "+i;
            redisTemplate.opsForList().rightPush("stock:list",stockName);
        }
        return "ok";
    }



    /**
     * @description: 卖出商品，有库存就减1 没有库存就打印无库存日志
     * @author: Xiang想
     * @date: 2022/9/5 5:02 PM
     * @param: []
     * @return: java.lang.String
     **/
    @RequestMapping("/deductStock")
    public String deductStock(){
        int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
        if (stock>0){
            int realStock = stock - 1;
            stringRedisTemplate.opsForValue().set("stock",String.valueOf(realStock));
            System.out.println("商品扣减成功，剩余商品："+realStock);
        }else {
            System.out.println("库存不足....");
        }
        return "end";
    }

    /**
     * @description: 在卖出的服务中添加 synchronized 关键字 添加锁
     * @author: Xiang想
     * @date: 2022/9/5 5:02 PM
     * @param: []
     * @return: java.lang.String
     **/
    @RequestMapping("/syncDeductStock")
    public String syncDeductStock(){
        synchronized (this){
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
            if (stock>0){
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set("stock",String.valueOf(realStock));
                System.out.println("服务A  商品扣减成功，剩余商品："+realStock);
            }else {
                System.out.println("库存不足....");
            }
            return "end";
        }
    }

}
