package com.liuyuncen.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.controller
 * @author: Xiang想
 * @createTime: 2022-09-19  10:21
 * @description: TODO
 * @version: 1.0
 */
@RestController
@Slf4j
@RequestMapping("/queue/")
public class SendController {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @GetMapping("/send")
    public String sendMsg(){
        String uuid = UUID.randomUUID().toString();
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sim.format(new Date());
        String msg = "你好！ "+date+" -  "+uuid;
        String channel = "channel";
        log.info("生产者 -----  " + msg);
        stringRedisTemplate.convertAndSend(channel, msg);
        return "OK!";
    }
}
