package com.liuyuncen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.config
 * @author: Xiang想
 * @createTime: 2022-09-19  10:19
 * @description: TODO
 * @version: 1.0
 */
@Configuration
public class RedisTemplateConfig {
    /**
     * @description: 使用默认的工厂初始化redis操作模板
     * @author: Xiang想
     * @date: 2022/9/19 10:19 AM
     * @param: [connectionFactory]
     * @return: org.springframework.data.redis.core.StringRedisTemplate
     **/
    @Bean
    StringRedisTemplate template(RedisConnectionFactory connectionFactory) {
        return new StringRedisTemplate(connectionFactory);
    }


}
