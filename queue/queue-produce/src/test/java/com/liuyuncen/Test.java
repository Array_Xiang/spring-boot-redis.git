package com.liuyuncen;

import lombok.extern.slf4j.Slf4j;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-19  09:49
 * @description: TODO
 * @version: 1.0
 */
@Slf4j
public class Test {

    public static void main(String[] args) {
        log.info("这是Info日志");
        log.error("这是Error日志");
    }
}
