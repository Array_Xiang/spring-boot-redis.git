package com.liuyuncen.receiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @belongsProject: redis_springboot
 * @belongsPackage: com.liuyuncen.receiver
 * @author: Xiang想
 * @createTime: 2022-09-19  10:57
 * @description: TODO
 * @version: 1.0
 */
@Slf4j
@Component
public class RedisReceiver {

    /**
     * @description: 接收 Redis 消息
     * @author: Xiang想
     * @date: 2022/9/19 10:58 AM
     * @param: [message]
     * @return: void
     **/
    public void getMsg(String message){
        log.info("消费者 -----  "+message);
    }

}
