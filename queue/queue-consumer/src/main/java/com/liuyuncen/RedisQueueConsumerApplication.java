package com.liuyuncen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-19  09:44
 * @description: TODO
 * @version: 1.0
 */
@SpringBootApplication
public class RedisQueueConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisQueueConsumerApplication.class,args);
    }
}
