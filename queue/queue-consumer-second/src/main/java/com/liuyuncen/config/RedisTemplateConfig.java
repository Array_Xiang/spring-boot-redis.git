package com.liuyuncen.config;

import com.liuyuncen.receiver.RedisReceiver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.config
 * @author: Xiang想
 * @createTime: 2022-09-19  10:19
 * @description: TODO
 * @version: 1.0
 */
@Configuration
public class RedisTemplateConfig {
    /**
     * @description: 使用默认的工厂初始化redis操作模板
     * @author: Xiang想
     * @date: 2022/9/19 10:19 AM
     * @param: [connectionFactory]
     * @return: org.springframework.data.redis.core.StringRedisTemplate
     **/
    @Bean
    StringRedisTemplate template(RedisConnectionFactory connectionFactory) {
        return new StringRedisTemplate(connectionFactory);
    }

    /**
     * @description: 初始化监听器
     * @author: Xiang想
     * @date: 2022/9/19 10:59 AM
     * @param: [connectionFactory, listenerAdapter]
     * @return: org.springframework.data.redis.listener.RedisMessageListenerContainer
     **/
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();

        container.setConnectionFactory(connectionFactory);
        // 每条消息都需要从相同或不同的管道进入，此处只配置一个管道，如果需要多个消息订阅处理，则需要配置多个
        container.addMessageListener(listenerAdapter, new PatternTopic("channel"));

        return container;
    }

    /**
     * @description: 指定对象中的指定处理方法  对应 RedisReceiver 类中的 getMsg(String message)
     * @author: Xiang想
     * @date: 2022/9/19 11:00 AM
     * @param: [redisReceiver]
     * @return: org.springframework.data.redis.listener.adapter.MessageListenerAdapter
     **/
    @Bean
    MessageListenerAdapter listenerAdapter(RedisReceiver redisReceiver) {
        return new MessageListenerAdapter(redisReceiver, "getMsg");
    }
}
