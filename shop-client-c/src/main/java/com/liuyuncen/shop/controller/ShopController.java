package com.liuyuncen.shop.controller;

import org.redisson.Redisson;
import org.redisson.RedissonRedLock;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.shop.controller
 * @author: Xiang想
 * @createTime: 2022-09-06  10:50
 * @description: TODO
 * @version: 1.0
 */
@RestController
public class ShopController {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate<String,Object> redisTemplate;

    @Autowired
    Redisson redisson;

    /**
     * @description: 在卖出的服务中添加 synchronized 关键字 添加锁
     * @author: Xiang想
     * @date: 2022/9/5 5:02 PM
     * @param: []
     * @return: java.lang.String
     **/
    @GetMapping("/syncDeductStock")
    public String syncDeductStock(){
        synchronized (this){
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
            if (stock>0){
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set("stock",String.valueOf(realStock));
                System.out.println("服务C  商品扣减成功，剩余商品："+realStock);
            }else {
                System.out.println("库存不足....");
            }
            return "end";
        }
    }

    /**
     * @description: Redis 实现分布式锁
     * @author: Xiang想
     * @date: 2022/9/6 11:15 AM
     * @param: []
     * @return: java.lang.String
     **/
    @GetMapping("/redisLockStock")
    public String redisLockStock(){
        // 创建一个key，保存至redis
        String key = "lock";
        String lock_value = UUID.randomUUID().toString();
        // setnx
        //让设置key和设置key的有效时间都可以同时执行
        boolean result = stringRedisTemplate.opsForValue().setIfAbsent(key, lock_value, 10, TimeUnit.SECONDS);
        // 当不存在key时，可以设置成功，回执true；如果存在key，则无法设置，返回false
        if (!result) {
            // 前端监测，redis中存在，则不能让这个抢购操作执行，予以提示！
            return "err";
        }
        try {
            // 获取Redis数据库中的商品数量
            Integer stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
            // 减库存
            if (stock > 0) {
                int realStock = stock - 1;
                stringRedisTemplate.opsForValue().set("stock", String.valueOf(realStock));
                System.out.println("商品扣减成功，剩余商品：" + realStock);
            } else {
                System.out.println("库存不足.....");
            }
        } finally {
            // 程序执行完成，则删除这个key
            // 放置于finally中，保证即使上述逻辑出问题，也能del掉

            // 判断redis中该数据是否是这个接口处理时的设置的，如果是则删除
            if(lock_value.equalsIgnoreCase(stringRedisTemplate.opsForValue().get(key))) {
                stringRedisTemplate.delete(key);
            }
        }
        return "end";
    }

    @GetMapping("/redisListLockStock")
    public String redisListLockStock(){
        String key = "stock:list";
        String stock = String.valueOf(redisTemplate.opsForList().leftPop(key));
        if ("null".equals(stock)){
            System.out.println("C服务 库存为空...");
        }else{
            System.out.println("C服务 消费库存:"+stock);
        }
        return "end";
    }


    /**
     * @description: redisson 实现分布式锁
     * @author: Xiang想
     * @date: 2022/9/8 9:16 AM
     * @param: []
     * @return: java.lang.String
     **/
    @GetMapping("/redissonLockStock")
    public String redissonLockStock() throws InterruptedException {
        String key = "lock";
        RLock lock = redisson.getLock(key);
        try {
            lock.lock();
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
            if (stock % 100 == 0){
                System.out.println("C服务 延迟10秒");
                Thread.sleep(40000);
                System.out.println("C服务 延迟10秒结束 继续后续操作");
            }
            if (stock>0){
                int resultStock = stock - 1;
                stringRedisTemplate.opsForValue().set("stock",String.valueOf(resultStock));
                System.out.println("C服务 商品扣除成功，剩余商品:"+resultStock);
            }else {
                System.out.println("C服务 库存不足...");
            }
        }finally {
            lock.unlock();
        }
        return "end";
    }



    @GetMapping("/redLockStock")
    public String redLockStock(){
        // 创建多个key，
        String key1 = "lock:1";
        String key2 = "lock:2";
        String key3 = "lock:3";
        RLock lock1 = redisson.getLock(key1);
        RLock lock2 = redisson.getLock(key2);
        RLock lock3 = redisson.getLock(key3);

        RedissonRedLock redLock = new RedissonRedLock(lock1, lock2, lock3);
        try {
            boolean tryLock = redLock.tryLock(10, 30, TimeUnit.SECONDS);
            if (tryLock){
                int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
                if (stock>0){
                    int resultStock = stock - 1;
                    stringRedisTemplate.opsForValue().set("stock",String.valueOf(resultStock));
                    System.out.println("C服务 商品扣除成功，剩余商品:"+resultStock);
                }else {
                    System.out.println("C服务 库存不足...");
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            redLock.unlock();
        }
        return "end";
    }
}
