package com.liuyuncen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen
 * @author: Xiang想
 * @createTime: 2022-09-06  10:55
 * @description: TODO
 * @version: 1.0
 */
@SpringBootApplication
public class ShopClientCApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopClientCApplication.class,args);
    }
}
