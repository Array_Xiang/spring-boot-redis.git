package com.liuyuncen.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.entity
 * @author: Xiang想
 * @createTime: 2022-09-09  14:20
 * @description: TODO
 * @version: 1.0
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String passwd;
    private String sex;
}
