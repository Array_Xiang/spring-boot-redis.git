package com.liuyuncen.service;

import com.liuyuncen.entity.User;
import com.liuyuncen.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.service
 * @author: Xiang想
 * @createTime: 2022-09-09  15:04
 * @description: TODO
 * @version: 1.0
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    public List<User> queryUserList(){
        return userMapper.queryUser();
    }

    @Cacheable(cacheNames = "userCache",key = "#id")
    public User queryUserById(String id){
        return userMapper.queryById(id);
    }
}
