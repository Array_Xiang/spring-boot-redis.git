package com.liuyuncen.config;

import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @belongsProject: radis_springboot Redis 相关配置文件
 * @belongsPackage: com.liuyuncen.config
 * @author: Xiang想
 * @createTime: 2022-09-09  13:57
 * @description: TODO
 * @version: 1.0
 */
@Configuration
public class RedisConfig {


    /**
     * @description: 重写Redis序列化定义方式，采取Json方式，避免Json格式乱码
     * @author: Xiang想
     * @date: 2022/9/9 1:58 PM
     * @param: [factory]
     * @return: org.springframework.data.redis.core.RedisTemplate<java.lang.String,java.lang.Object>
     **/
    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(factory);
        // 创建json序列化对象
        GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        // 设置key序列化String
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        // 设置value序列化 json
        redisTemplate.setValueSerializer(genericJackson2JsonRedisSerializer);
        // 设置hash key序列化String
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        // 设置hash value 序列化json
        redisTemplate.setHashValueSerializer(genericJackson2JsonRedisSerializer);

        // 初始化redis完成序列化的方法
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    /**
     * @description: 缓存配置
     * @author: Xiang想
     * @date: 2022/9/9 2:01 PM
     * @param: [factory]
     * @return: org.springframework.cache.CacheManager
     **/
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory){
        RedisCacheConfiguration cacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
//                .entryTtl(Duration.ofMillis(30))
                .disableCachingNullValues()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
        return RedisCacheManager.builder(factory).cacheDefaults(cacheConfiguration).build();
    }
}
