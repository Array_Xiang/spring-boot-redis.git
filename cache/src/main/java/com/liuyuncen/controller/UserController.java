package com.liuyuncen.controller;

import com.liuyuncen.entity.User;
import com.liuyuncen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.controller
 * @author: Xiang想
 * @createTime: 2022-09-09  14:23
 * @description: TODO
 * @version: 1.0
 */
@RestController
@RequestMapping("/user")
@CacheConfig(cacheNames = "user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/queryUserList")
    public List<User> queryUserList(){
        return userService.queryUserList();
    }

    @GetMapping("/queryUserById")
    public User queryUserById(String id){
        return userService.queryUserById(id);
    }
}
