package com.liuyuncen.mapper;

import com.liuyuncen.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @belongsProject: radis_springboot
 * @belongsPackage: com.liuyuncen.mapper
 * @author: Xiang想
 * @createTime: 2022-09-09  14:21
 * @description: TODO
 * @version: 1.0
 */
@Mapper
public interface UserMapper {
    @Select("select * from user")
    List<User> queryUser();

    @Select("select * from user where id = #{id}")
    User queryById(String id);
}
